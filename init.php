<?php defined('SYSPATH') or die('No direct script access.');

Route::set('uicategory', 'admin/uicategory(/<action>(/<id>(/<parent_id>)))')
    ->defaults(array(
    'controller'	=> 'UiCategory',
    'action'		=> 'index',
));

Route::set('uiproduct', 'admin/uiproduct(/<action>(/<id>(/<parent_id>)))')
    ->defaults(array(
    'controller'	=> 'UiProduct',
    'action'		=> 'index',
));


