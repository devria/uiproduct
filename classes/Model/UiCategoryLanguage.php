<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Raphael Azeredo
 */
class Model_UiCategoryLanguage extends ORM {

    protected $_table_name      = 'ui_category_language';
    protected $_sorting         = array('title' => 'ASC');

    protected $_belongs_to      = array(
        'ui_language'           => array('model' => 'UiLanguage', 'foreign_key' => 'ui_language_id'),
        'ui_category'           => array('model' => 'UiCategory', 'foreign_key' => 'ui_category_id')
    );
}