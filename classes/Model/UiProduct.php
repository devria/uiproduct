<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Raphael Azeredo
 */
class Model_UiProduct extends ORM {

    protected $_table_name      = 'ui_product';
    protected $_sorting         = array('id' => 'ASC');

    protected $_has_many        = array(
        'ui_product_language'   => array()
    );
}