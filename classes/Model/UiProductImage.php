<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Raphael Azeredo
 */
class Model_UiProductImage extends ORM {

    protected $_table_name      = 'ui_product_image';
    protected $_sorting         = array('position' => 'ASC');

    protected $_belongs_to      = array(
        'ui_product'            => array('model' => 'UiProduct', 'foreign_key' => 'ui_product_id')
    );
}