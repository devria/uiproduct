<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Raphael Azeredo
 */
class Model_UiProductLanguage extends ORM {

    protected $_table_name      = 'ui_product_language';
    protected $_sorting         = array('title' => 'ASC');

    protected $_belongs_to      = array(
        'ui_language'           => array('model' => 'UiLanguage', 'foreign_key' => 'ui_language_id'),
        'ui_category'           => array('model' => 'UiCategory', 'foreign_key' => 'ui_category_id'),
        'ui_product'            => array('model' => 'UiProduct', 'foreign_key' => 'ui_product_id')
    );

    public function upload($field, $ui_product_id, $image_name)
    {
        if($_FILES[$field]['error'] == 0)
        {
            //Realiza as validações
            if (
                Upload::valid($_FILES[$field]) &&
                Upload::not_empty($_FILES[$field]) &&
                Upload::type($_FILES[$field], array('jpg', 'jpeg', 'png')))
            {
                //Destino
                $path_large	    = DOCROOT .'uploads/product/'. $ui_product_id .'/large';
                $path_small	    = DOCROOT .'uploads/product/'. $ui_product_id .'/small';
                $image_large        = $path_large .'/'. $image_name;
                $image_small        = $path_small .'/'. $image_name;
                $image_original     = DOCROOT .'uploads/product/'. $ui_product_id .'/'. $image_name;

                // Verificando se pasta de upload existe
                if(!file_exists($path_large))
                    mkdir($path_large,0777,true);

                if(!file_exists($path_small))
                    mkdir($path_small,0777,true);

                // Gravando arquivo
                move_uploaded_file($_FILES[$field]['tmp_name'], $image_original);

                $imageSize = getimagesize($image_original);
                $width = $imageSize[0];
                $height = $imageSize[1];

                if($width > 800 && $height > 600)
                {
                    //Realiza o crop da imagem
                    Image::factory($image_original)
                        ->resize(800, 600, Image::AUTO)
                        ->save($image_large);
                }
                else{
                    copy($image_original, $image_large);
                }

                if($width > 180 && $height > 130)
                {
                    //Realiza o crop da imagem
                    Image::factory($image_original)
                        ->resize(180, 130, Image::AUTO)
                        ->save($image_small);
                }
                else{
                    copy($image_original, $image_small);
                }

                // Chmod no arauivo
                @chmod($image_large,0777);
                @chmod($image_small,0777);
            }
        }
    }
}