<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Raphael Azeredo
 */
class Model_UiCategory extends ORM {

    protected $_table_name      = 'ui_category';
    protected $_sorting         = array('id' => 'ASC');

    protected $_has_many        = array(
        'ui_category_language'  => array()
    );
}