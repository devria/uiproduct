<?php defined('SYSPATH') or die('No direct script access.');

class Controller_UiProduct extends Controller_Admin_Access  {

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        //Recupera o objeto do e acordo com o id informado
        $ui_product_language = ORM::factory('UiProductLanguage')
                                ->with('ui_product')
                                ->with('ui_language')
                                ->where('ui_language_id', '=', '1')
                                ->find_all();

        //Recupera o objeto do e acordo com o id informado
        $ui_language = ORM::factory('UiLanguage')->find_all();

        //Renderiza a view
        $this->template->content = View::factory('UiProduct/Index')
             ->set('ui_product_language', $ui_product_language)
             ->set('ui_language', $ui_language);
    }

    public function action_add()
    {
        $this->action_form();
    }

    public function action_edit()
    {
        $this->action_form();
    }

    public function action_form()
    {
        //Define o array de erros
        $errors = array();

        //Recupera o id informado, caso não exista retorna 0
        $ui_product_id = $this->request->param('id', 0);

        //Recupera o id informado, caso não exista retorna 0
        $ui_language_id = $this->request->param('parent_id', '1');

        //Recupera o objeto do e acordo com o id informado
        $ui_product = ORM::factory('UiProduct', $ui_product_id);

        //Recupera o objeto do e acordo com o id informado
        $ui_product_language = ORM::factory('UiProductLanguage')
                           ->where('ui_product_id', '=', $ui_product_id)
                           ->where('ui_language_id', '=', $ui_language_id)
                           ->find();

        //Recupera o objeto do e acordo com o id informado
        $ui_language = ORM::factory('UiLanguage')->where('id', '=', $ui_language_id)->find();

        //Recebe os dados que vieram via POST
        $params = $this->request->post();

        //Verifica se recebeu algum dado
        if ($params)
        {
            //Realiza a validação dos dados
            $validate = Validation::factory($params);
            $validate->rule('title','not_empty');

            if ($validate->check()){

                if(!empty($params['active'])) $params['active'] = '1'; else $params['active'] = '0';

                $ui_product->values($params);
                $ui_product->save();

                $ui_product_language->values($params);
                $ui_product_language->ui_product_id = $ui_product->id;

                if($ui_product_language->save())
                    $this->redirect($this->module_link);
            } else {
                //Recupera os erros
                $errors = $validate->errors('register');
            }
        }
        
        //Recupera o objeto do e acordo com o id informado
        $ui_category_language = ORM::factory('UiCategoryLanguage')
                                ->where('ui_language_id', '=', '1')
                                ->where('active', '=', '1')
                                ->find_all()
                                ->as_array('ui_category_id', 'title');
        
        $ui_category_language[''] = 'Selecione...';
        ksort($ui_category_language);
        
        //Renderiza a view
        $this->template->content = View::factory('UiProduct/Form')
             ->set('errors', $errors)
             ->set('ui_language', $ui_language)
             ->set('ui_product_language', $ui_product_language)
             ->set('ui_category_language', $ui_category_language);
    }

    public function action_remove()
    {
        //Recupera o id informado, caso não exista retorna 0
        $id = $this->request->param('id', 0);

        //Recupera o objeto do e acordo com o id informado
        $ui_product = ORM::factory('UiProduct', $id);

        //Recupera o objeto do e acordo com o id informado
        $ui_product_language = ORM::factory('UiProductLanguage')
                                ->where('ui_product_id', '=', $ui_product->id)
                                ->where('ui_language_id', '=', '1')
                                ->find();

        //Recebe os dados que vieram via POST
        $params = $this->request->post();

        //Verifica se recebeu algum dado
        if ($params)
        {
            $ui_product_language = ORM::factory('UiProductLanguage')
                                    ->where('ui_product_id', '=', $ui_product->id)
                                    ->find_all();

            foreach($ui_product_language as $item_ui_product_language)
            {
                //Apaga o conteúdo
                $item_ui_product_language->delete();
            }

            if($ui_product->delete())
                $this->redirect($this->module_link);
        }

        //Renderiza a view
        $this->template->content = View::factory('UiProduct/Remove')
             ->set('ui_product_language', $ui_product_language);
    }
    
    public function action_images()
    {
        $errors = array();
        
        //Recupera o id informado, caso não exista retorna 0
        $id = $this->request->param('id', 0);
        
        //Recupera o objeto do e acordo com o id informado
        $ui_product_language = ORM::factory('UiProductLanguage')
                               ->where('ui_language_id', '=', '1')
                               ->where('ui_product_id', '=', $id)
                               ->find();

        //Recebe os dados que vieram via POST
        $params = $this->request->post();

        //Verifica se recebeu algum dado
        if ($params)
        {
            $dir = DOCROOT .'product/'. $id;
            
            //Realiza as validações
            if (
                Upload::valid($_FILES['image']) &&
                Upload::not_empty($_FILES['image']) &&
                Upload::type($_FILES['image'], array('jpg', 'jpeg', 'png')))
            {
                // Verificando se pasta de upload existe
                if(!file_exists($dir))
                    mkdir($dir,0777,true);

                if(!file_exists($dir .'/small'))
                    mkdir($dir .'/small',0777,true);
                
                if(!file_exists($dir .'/medium'))
                    mkdir($dir .'/medium',0777,true);
                
                if(!file_exists($dir .'/large'))
                    mkdir($dir .'/large',0777,true);
                
                $filename       = md5(uniqid(time())) .'.'. pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                $image_original = $dir .'/'. $filename;
                $image_small    = $dir .'/small/'. $filename;
                $image_medium   = $dir .'/medium/'. $filename;
                $image_large    = $dir .'/large/'. $filename;

                // Gravando arquivo
                move_uploaded_file($_FILES['image']['tmp_name'],$image_original);

                //Realiza o crop da imagem
                Image::factory($image_original)
                       ->resize(300, 194, Image::NONE)
                       ->save($image_small);
                
                //Realiza o crop da imagem
                Image::factory($image_original)
                       ->resize(460, 297, Image::NONE)
                       ->save($image_medium);
                
                //Realiza o crop da imagem
                Image::factory($image_original)
                       ->resize(600, 387, Image::NONE)
                       ->save($image_large);
                
                //Recupera o objeto do e acordo com o id informado
                $ui_product_image = ORM::factory('UiProductImage')
                                           ->where('ui_product_id', '=', $id)
                                           ->find_all();
                
                $total_images = $ui_product_image->count();
                $ui_product_image = ORM::factory('UiProductImage');
                if($total_images == 0)
                {
                    $ui_product_image->highlight = 1;
                }
                
                $ui_product_image->ui_product_id = $id;
                $ui_product_image->filename = $filename;
                $ui_product_image->position = ($total_images + 1);
                $ui_product_image->save();
                
                $this->redirect($this->module_link .'/images/'. $id);
            }
            else{
                array_push($errors, 'A imagem deve ser no formato: jpg, jpeg ou png');
            }
        }
        
        $ui_product_image = ORM::factory('UiProductImage')
                                   ->where('ui_product_id', '=', $id)
                                   ->find_all();

        //Renderiza a view
        $this->template->content = View::factory('UiProduct/Images')
             ->set('errors', $errors)
             ->set('ui_product_language', $ui_product_language)
             ->set('ui_product_image', $ui_product_image);
    }
    
    public function action_highlight()
    {
        //Recupera o id informado, caso não exista retorna 0
        $ui_product_id = $this->request->param('id', 0);
        
        $ui_product_image_id = $this->request->param('parent_id', 0);
        
        DB::update('ui_product_image')->set(array('highlight' => '0'))->where('ui_product_id', '=', $ui_product_id)->execute();

        $ui_product_image = ORM::factory('UiProductImage', $ui_product_image_id);
        $ui_product_image->highlight = 1;
        if($ui_product_image->save())
            $this->redirect($this->module_link .'/images/'. $ui_product_id);
    }
    
    public function action_savelegend() {
        $this->auto_render = false;

        if (!$this->request->is_ajax())
            die('Acesso negado');

        $params = $this->request->post();

        if ($params) {
            $ui_product_image = ORM::factory('UiProductImage', $params['id']);
            $ui_product_image->legend = $params['legend'];

            return $ui_product_image->save();
        }
    }
    
    public function action_saveorder() {
        $this->auto_render = false;

        if (!$this->request->is_ajax())
            die('Acesso negado');

        $params = $this->request->post();

        if ($params) {
            $order = $params['order'];
            if (count($order) > 0) {
                foreach($order as $key => $id)
                {
                    $ui_product_image = ORM::factory('UiProductImage', $id);
                    if($ui_product_image->loaded())
                    {
                        $ui_product_image->position = ($key+1);
                        $ui_product_image->save();
                    }
                }
            }
        }
    }
    
    public function action_removeimage() {
        $this->auto_render = false;

        if (!$this->request->is_ajax())
            die('Acesso negado');

        $params = $this->request->post();

        if ($params) 
        {
            $ui_product_image = ORM::factory('UiProductImage', $params['id']);
            
            if($ui_product_image->loaded())
            {
                $dir = DOCROOT .'product/'. $ui_product_image->id;

                $filename       = $ui_product_image->filename;
                @unlink($dir .'/'. $filename);
                @unlink($dir .'/small/'. $filename);
                @unlink($dir .'/medium/'. $filename);
                @unlink($dir .'/large/'. $filename);
                
                $ui_product_image->delete();
            }
        }
    }
}