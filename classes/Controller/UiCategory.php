<?php defined('SYSPATH') or die('No direct script access.');

class Controller_UiCategory extends Controller_Admin_Access  {

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        //Recupera o objeto do e acordo com o id informado
        $ui_category_language = ORM::factory('UiCategoryLanguage')
                                ->with('ui_category')
                                ->with('ui_language')
                                ->where('ui_language_id', '=', '1')
                                ->find_all();

        //Recupera o objeto do e acordo com o id informado
        $ui_language = ORM::factory('UiLanguage')->find_all();

        //Renderiza a view
        $this->template->content = View::factory('UiCategory/Index')
             ->set('ui_category_language', $ui_category_language)
             ->set('ui_language', $ui_language);
    }

    public function action_add()
    {
        $this->action_form();
    }

    public function action_edit()
    {
        $this->action_form();
    }

    public function action_form()
    {
        //Define o array de erros
        $errors = array();

        //Recupera o id informado, caso não exista retorna 0
        $ui_category_id = $this->request->param('id', 0);

        //Recupera o id informado, caso não exista retorna 0
        $ui_language_id = $this->request->param('parent_id', '1');

        //Recupera o objeto do e acordo com o id informado
        $ui_category = ORM::factory('UiCategory', $ui_category_id);

        //Recupera o objeto do e acordo com o id informado
        $ui_category_language = ORM::factory('UiCategoryLanguage')
                           ->where('ui_category_id', '=', $ui_category_id)
                           ->where('ui_language_id', '=', $ui_language_id)
                           ->find();

        //Recupera o objeto do e acordo com o id informado
        $ui_language = ORM::factory('UiLanguage')->where('id', '=', $ui_language_id)->find();

        //Recebe os dados que vieram via POST
        $params = $this->request->post();

        //Verifica se recebeu algum dado
        if ($params)
        {
            //Realiza a validação dos dados
            $validate = Validation::factory($params);
            $validate->rule('title','not_empty');

            if ($validate->check()){

                if(!empty($params['active'])) $params['active'] = '1'; else $params['active'] = '0';

                $ui_category->values($params);
                $ui_category->save();

                $ui_category_language->values($params);
                $ui_category_language->ui_category_id = $ui_category->id;

                if($ui_category_language->save())
                    $this->redirect($this->module_link);
            } else {
                //Recupera os erros
                $errors = $validate->errors('register');
            }
        }

        //Renderiza a view
        $this->template->content = View::factory('UiCategory/Form')
             ->set('errors', $errors)
             ->set('ui_language', $ui_language)
             ->set('ui_category_language', $ui_category_language);
    }

    public function action_remove()
    {
        //Recupera o id informado, caso não exista retorna 0
        $id = $this->request->param('id', 0);

        //Recupera o objeto do e acordo com o id informado
        $ui_category = ORM::factory('UiCategory', $id);

        //Recupera o objeto do e acordo com o id informado
        $ui_category_language = ORM::factory('UiCategoryLanguage')
                                ->where('ui_category_id', '=', $ui_category->id)
                                ->where('ui_language_id', '=', '1')
                                ->find();

        //Recebe os dados que vieram via POST
        $params = $this->request->post();

        //Verifica se recebeu algum dado
        if ($params)
        {
            $ui_category_language = ORM::factory('UiCategoryLanguage')
                                    ->where('ui_category_id', '=', $ui_category->id)
                                    ->find_all();

            foreach($ui_category_language as $item_ui_category_language)
            {
                //Apaga o conteúdo
                $item_ui_category_language->delete();
            }

            if($ui_category->delete())
                $this->redirect($this->module_link);
        }

        //Renderiza a view
        $this->template->content = View::factory('UiCategory/Remove')
             ->set('ui_category_language', $ui_category_language);
    }
}