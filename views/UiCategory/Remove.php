<?php defined('SYSPATH') or die('No direct script access.'); ?>

<section id="forms">
    <div class="row">
        <div class="span10 offset1">
            <form id="formNews" method="post" class="form-horizontal well">
                <fieldset>
                    <legend>Gerenciador de categorias</legend>
                    <div class="control-group">
                        <label class="control-label"><strong>Idioma</strong></label>
                        <div class="control-remove">
                            <?php if(!empty($ui_category_language->ui_language->icon)): ?>
                                <img src="<?= url::site() .'uploads/icons/'. $ui_category_language->ui_language->icon ?>">
                            <?php endif; ?>
                            <?= $ui_category_language->ui_language->name ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Título</strong></label>
                        <div class="control-remove">
                            <?= $ui_category_language->title ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><strong>Ativo</strong></label>
                        <div class="control-remove">
                            <?php if($ui_category_language->active == '1') echo 'Sim'; else echo 'Não'; ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <p>Tem certeza que deseja remover esse registro?</p>
                        <?= Form::submit('btRemove', 'Sim', array('class' => 'btn btn-success')) ?>
                        <?= HTML::anchor($module_link, '<button type="button" class="btn btn-danger">Não</button>'); ?>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</section>