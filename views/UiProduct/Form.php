<?php defined('SYSPATH') or die('No direct script access.'); ?>

<section id="forms">
    <?php if (count($errors) > 0): ?>
        <div class="alert alert-error">
            <?php foreach($errors as $k => $error): ?>
            <p><?= $error; ?></p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="span10 offset1">
            <?= Form::open(null, array('method' => 'post', 'class' => 'form-horizontal well', 'id' => 'formProduct', 'enctype' => 'multipart/form-data')) ?>
                <fieldset>
                    <legend>Gerenciador de produtos</legend>
                    <div class="control-group">
                        <label class="control-label">Idioma</label>
                        <div class="control-remove" style="margin-left: 200px;">
                            <?php if(!empty($ui_language->icon)): ?>
                                <img src="<?= url::site() .'uploads/icons/'. $ui_language->icon ?>">
                            <?php endif; ?>
                            <?= $ui_language->name ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="category">Categoria</label>
                        <div class="controls">
                            <?= Form::select('ui_category_id', $ui_category_language, $ui_product_language->ui_category_id, array('class' => 'required')) ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="title">Título</label>
                        <div class="controls">
                            <?= Form::input('title', $ui_product_language->title, array('class' => 'input-xlarge required')) ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="reference">Referência</label>
                        <div class="controls">
                            <?= Form::input('reference', $ui_product_language->reference, array('class' => 'input-medium')) ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="description">Descrição</label>
                        <div class="controls">
                            <?= Form::textarea('description', $ui_product_language->description, array('id' => 'description', 'style' => 'width: 550px; height: 200px;')) ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="active">Ativo</label>
                        <div class="controls">
                            <?= Form::checkbox('active', '1', ($ui_product_language->active == '1')) ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <?= Form::hidden('ui_language_id', $ui_language->id) ?>
                        <?= Form::button('btSave', 'Salvar', array('type' => 'submit', 'class' => 'btn btn-success')) ?>
                        <?= HTML::anchor($module_link, '<button type="button" class="btn btn-danger">Cancelar</button>'); ?>
                    </div>
                </fieldset>
            <?= Form::close() ?>
        </div>
    </div>
</section>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $( "#date" ).datepicker();
        $("#formProduct").validate({
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                    $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                    $(element).parents('.control-group').removeClass('error');
                    $(element).parents('.control-group').addClass('success');
            }
        });
    });
</script>