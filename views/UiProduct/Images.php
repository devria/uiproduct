<?php defined('SYSPATH') or die('No direct script access.'); ?>
<section id="tables">
    <h1><?= Ui_Util::truncate($ui_product_language->title, 50) ?></h1>
    <hr>
    <div>
        <?php if (count($errors) > 0): ?>
            <div class="alert alert-error">
                <?php foreach($errors as $k => $error): ?>
                <p><?= $error; ?></p>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?= Form::open(null, array('method' => 'post', 'class' => 'form-horizontal well', 'id' => 'formImages', 'enctype' => 'multipart/form-data')) ?>
            <span>Selecione a imagem para enviar: </span>
            <?= Form::file('image') ?>
            <?= Form::button('btSendImage', 'Enviar imagem', array('type' => 'submit', 'class' => 'btn btn-success')) ?>
            <?= HTML::anchor($module_link, '<button type="button" class="btn btn-danger">Retornar a listagem de ofertas</button>'); ?>
        <?= Form::close() ?>
    </div>
    <ul class="thumbnails" id="sortable">
        <?php foreach ($ui_product_image as $image): ?>
            <li class="span3" id="<?= $image->id ?>">
                <div class="thumbnail">
                    <img src="<?= URL::site('product/'. $image->ui_product_id .'/small/'. $image->filename) ?>" alt="ALT NAME">
                    <div class="caption">
<!--                        <input type="text" placeholder="Legenda" class="legend" title="<?= $image->id ?>" value="<?= $image->legend ?>"/>-->
                        <?php if($image->highlight == 1): ?>
                            <p align="center"><a class="btn btn-primary btn-block disabled">Destaque</a></p>
                            <p align="center"><a class="btn btn-danger btn-block disabled">Remover</a></p>
                        <?php else: ?>
                            <p align="center"><a href="<?= URL::site('admin/uiproduct/highlight/'. $image->ui_product_id .'/'. $image->id) ?>" class="btn btn-primary btn-block">Destaque</a></p>
                            <p align="center"><a id="<?= $image->id ?>" class="btn btn-danger btn-block pointer removeImage">Remover</a></p>
                        <?php endif; ?>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</section>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $('input[type=checkbox],input[type=radio],input[type=file]').uniform({fileBtnText: 'Selecionar arquivo', fileDefaultText: 'Nenhum arquivo selecionado'});
        
        $('.legend').blur(function(){
            var id = this.title;
            var legend = this.value;

            $.ajax({
                type: "POST",
                url: "<?php echo url::site('admin/uiproduct/savelegend') ?>",
                data: {id:id, legend:legend},
                success: alert('Legenda alterada com sucesso!')
            });
        });
        
        $('.removeImage').on('click', function(){
            var id = this.id;
            confirm("Tem certeza que deseja excluir a foto selecionada?", function(confirmed) {
                if(confirmed)
                {
                    $.ajax({
                        type: "POST",
                        url: "<?= url::site('admin/uiproduct/removeimage') ?>",
                        data: {id:id},
                        success: document.location.reload(true)
                    });
                }
            });
        });
        
        $( "#sortable" ).sortable({
            update: function() {
                var order = $('#sortable').sortable('toArray');
                $.ajax({
                    type: "POST",
                    url: "<?php echo url::site('admin/uiproduct/saveorder') ?>",
                    data: {order:order},
                    success: alert('Ordenação alterada com sucesso!')
                });
            }
        });

        $( "#sortable" ).disableSelection();
    });
</script>